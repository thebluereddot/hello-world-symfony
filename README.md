# Hello World from Symfony

## Deployment

Deploy your application on Cloud Run


## Build Docker Image

Build Docker image

```bash
docker build --platform linux/amd64 . -t gcr.io/tbrd-lab/hello-world-symfony
```

Push Docker image to Docker Hub

```bash
docker push gcr.io/tbrd-lab/hello-world-symfony
```

Test your Docker image

```bash
docker run -p 80:80 gcr.io/tbrd-lab/hello-world-symfony
```

Deploy your Cloud Run service

```bash
gcloud run deploy hello-world-symfony --image gcr.io/tbrd-lab/hello-world-symfony --region europe-west3 --port 80
```