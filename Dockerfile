FROM php:8.2-apache

RUN apt-get update \
    && apt-get install -y vim

RUN docker-php-ext-install pdo
RUN apt-get install -y libzip-dev \
    && docker-php-ext-configure zip \
    && docker-php-ext-install zip

WORKDIR /var/www/project

ENV APP_ENV=prod

EXPOSE 80

COPY docker/000-default.conf /etc/apache2/sites-available/
COPY . /var/www/project/

RUN mkdir -p /var/www/project/var/log \
    && mkdir -p /var/www/project/var/cache \
    && chown -R www-data:www-data /var/www/ \
    && a2enmod rewrite

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php -r "if (hash_file('sha384', 'composer-setup.php') === 'e21205b207c3ff031906575712edab6f13eb0b361f2085f1f1237b7126d785e826a450292b6cfd1d64d92e6563bbde02') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" \
    && php composer-setup.php \
    && php -r "unlink('composer-setup.php');"

USER www-data

RUN php composer.phar install